﻿using System;
using System.Collections.Generic;
using System.Linq;
using _01;

namespace _07
{
    class Program
    {
        static void Main(string[] args)
        {

            using (Context db = new Context())
            {
                int id = 1;
                IEnumerable<Phone> enumerable = db.Phones;
                var phones = enumerable.Where(p => p.Id > id).ToList();
            }

            using (Context db = new Context())
            {
                int id = 1;
                IQueryable<Phone> queryable = db.Phones;
                var phones = queryable.Where(p => p.Id > id).ToList();
            }

            using (Context db = new Context())
            {
                IEnumerable<Phone> queryable = db.Phones;
                queryable = queryable.Where(p => p.CompanyId == 1);
                queryable = queryable.Where(p => p.Price < 50000);
                var phones = queryable.ToList();
            }

            Console.WriteLine("VovA");
        }
    }
}
