﻿using System;
using System.Linq;
using _01;

namespace _03
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Projection

            using (Context db = new Context())
            {
                var phones = db.Phones.Select(p => new
                {
                    Name = p.Name,
                    Price = p.Price,
                    Company = p.Company.Name
                });
                foreach (var phone in phones)
                    Console.WriteLine($"{phone.Name} ({phone.Price}) - {phone.Company}");
                Console.WriteLine(phones.First().GetType().Name);
                Console.WriteLine("---------------------");
            }

            using (Context db = new Context())
            {
                var phones = db.Phones.Select(p => new Model
                {
                    Name = p.Name,
                    Price = p.Price,
                    Company = p.Company.Name
                });
                foreach (Model phone in phones)
                    Console.WriteLine($"{phone.Name} ({phone.Price}) - {phone.Company}");
                Console.WriteLine(phones.First().GetType().Name);
                Console.WriteLine("---------------------");

            }

            #endregion

            #region Sorting

            Console.WriteLine("Sorting:\n");
            using (Context db = new Context())
            {
                var phones = db.Phones.OrderBy(p => p.Name);
                foreach (var phone in phones)
                    Console.WriteLine($"{phone.Id}.{phone.Name} ({phone.Price})");
            }
            Console.WriteLine("---------------------");

            using (Context db = new Context())
            {
                var phones = from p in db.Phones
                             orderby p.Name
                             select p;
                foreach (Phone phone in phones)
                    Console.WriteLine($"{phone.Id}.{phone.Name} ({phone.Price})");
            }
            Console.WriteLine("---------------------");

            using (Context db = new Context())
            {

                var phones = db.Phones.OrderByDescending(p => p.Name);

                foreach (Phone phone in phones)
                    Console.WriteLine($"{phone.Id}.{phone.Name} ({phone.Price})");
            }
            Console.WriteLine("---------------------");

            using (Context db = new Context())
            {
                var phones = db.Phones.OrderBy(p => p.Price).ThenBy(p => p.Company.Name);

                foreach (Phone phone in phones)
                    Console.WriteLine($"{phone.Id}.{phone.Name} ({phone.Price})");
            }
            #endregion

            Console.ReadKey();
        }
    }
}
