﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace _08
{
    class Program
    {
        static void Main(string[] args)
        {
            ////Generate();
            using Context db = new() { RoleId = 1 };
            var users = db.Users.Include(u => u.Role).ToList();
            foreach (var u in users)
                Console.WriteLine($"Name: {u.Name,-6} Age: {u.Age} Role: {u.Role?.Name}");
            Console.WriteLine("-----------");
            Console.WriteLine($"Min age: {db.Users.IgnoreQueryFilters().Min(x => x.Age)}");
            Console.WriteLine($"Min age with IgnoreQueryFilters: {db.Users.Min(x => x.Age)}");

            Console.WriteLine("-----------");
            Console.ReadKey();
        }
        static void Generate()
        {
            Role admin = new() { Name = "Admin" }, user = new() { Name = "User" };
            User u1 = new() { Name = "VovA", Age = 17, Role = user },
                 u2 = new() { Name = "Lena", Age = 18, Role = user },
                 u3 = new() { Name = "Sasha", Age = 19, Role = admin },
                 u4 = new() { Name = "Anna", Age = 20, Role = admin };
            using Context db = new();
            db.Roles.AddRange(user, admin);
            db.Users.AddRange(u1, u2, u3, u4);
            db.SaveChanges();
        }
    }
}
